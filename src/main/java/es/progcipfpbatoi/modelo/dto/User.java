package es.progcipfpbatoi.modelo.dto;

import java.time.LocalDate;

public class User {

    private int id;
    private String firstName;
    private String lastName;
    private String email;
    private String phoneNumber;
    private boolean active;
    private LocalDate createdOn;
    private int idEnterprise;
    private LocalDate birthday;

    public User(String firstName, String lastName, String email, String phoneNumber, boolean active, int idEnterprise, LocalDate birthday) {
        this(-1, firstName, lastName, email, phoneNumber, active,
                LocalDate.now(), idEnterprise, birthday);
    }

    public User(String firstName, String lastName, String email, boolean active, int idEnterprise, LocalDate birthday) {
        this(-1, firstName, lastName, email, "Sin teléfono", active,
                LocalDate.now(), idEnterprise, birthday);
    }

    public User(int id, String firstName, String lastName, String email, String phoneNumber, boolean active, LocalDate createdOn, int idEnterprise, LocalDate birthday) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.active = active;
        this.createdOn = createdOn;
        this.idEnterprise = idEnterprise;
        this.birthday = birthday;
    }

    public int getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public boolean isActive() {
        return active;
    }

    public LocalDate getCreatedOn() {
        return createdOn;
    }

    public int getIdEnterprise() {
        return idEnterprise;
    }

    public LocalDate getBirthday() {
        return birthday;
    }
}
