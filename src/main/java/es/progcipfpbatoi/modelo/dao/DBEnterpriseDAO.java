package es.progcipfpbatoi.modelo.dao;

import es.progcipfpbatoi.modelo.dto.Enterprise;
import es.progcipfpbatoi.services.MySqlConnectionService;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class DBEnterpriseDAO implements EnterpriseDAOInterface{
    @Override
    public List<Enterprise> findAll() {
        MySqlConnectionService service = new MySqlConnectionService(
                "localhost", "crm_db", "root", "123456");
        String sql = String.format("SELECT * FROM Enterprise");

        ArrayList<Enterprise>empresas = new ArrayList<>();

        try (
                Connection connection = service.getConnection();
                Statement statement = connection.createStatement();
                ResultSet resultSet = statement.executeQuery(sql);
        ) {

            while(resultSet.next()) {
                int id = resultSet.getInt("id");
                String name = resultSet.getString("name");
                String address = resultSet.getString("address");
                String city = resultSet.getString("city");
                String province = resultSet.getString("province");
                String country = resultSet.getString("country");
                LocalDate createdOn = resultSet.getDate("createdOn").toLocalDate();
                String nif = resultSet.getString("nif");

                Enterprise enterprise = new Enterprise(id, name, address, city, province, country,createdOn, nif);
                empresas.add(enterprise);
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return empresas;

    }

    @Override
    public Enterprise findById(int id) {
        return null;
    }

    @Override
    public void save(Enterprise enterprise) {

    }
}
