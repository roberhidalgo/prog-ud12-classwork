package es.progcipfpbatoi.modelo.dao;

import es.progcipfpbatoi.modelo.dto.Enterprise;

import java.util.List;

public interface EnterpriseDAOInterface {

    List<Enterprise>findAll();
    Enterprise findById(int id);
    void save(Enterprise enterprise);
}
