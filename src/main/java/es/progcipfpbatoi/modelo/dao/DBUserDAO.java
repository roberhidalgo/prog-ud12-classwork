package es.progcipfpbatoi.modelo.dao;

import es.progcipfpbatoi.modelo.dto.User;
import es.progcipfpbatoi.services.MySqlConnectionService;

import java.sql.*;
import java.time.LocalDate;

public class DBUserDAO implements UserDAOInterface{

    private static final String TABLE_NAME = "User";

    @Override
    public User findById(int id) {
        String sql = String.format("SELECT * FROM %s WHERE id=%d", TABLE_NAME, id);
        MySqlConnectionService connectionService = new MySqlConnectionService(
                "localhost", "crm_db", "root", "123456");

        User user = null;
        try ( Connection connection = connectionService.getConnection();
              Statement statement = connection.createStatement();
              ResultSet resultSet = statement.executeQuery(sql);) {

            while(resultSet.next()) {
                String firstName = resultSet.getString("firstName");
                String lastName = resultSet.getString("lastName");
                String email = resultSet.getString("email");
                String phoneNumber = resultSet.getString("phoneNumber");
                boolean active = intToBoolean(resultSet.getInt("active"));
                LocalDate createdOn = resultSet.getDate("createdOn").toLocalDate();
                int idEnterprise = resultSet.getInt("idEnterprise");
                LocalDate birthday = resultSet.getDate("birthday").toLocalDate();

                user = new User(id, firstName, lastName,
                        email, phoneNumber, active, createdOn,
                        idEnterprise, birthday);
            }
        }catch (SQLException e){
            e.printStackTrace();
        }

        return user;
    }

    private boolean intToBoolean(int valorInt) {
        return valorInt==1;
    }

    private int booleanToInt(boolean valorBoolean) {
        return valorBoolean?1:0;
    }

    private void insert(User user){
        String sql = String.format("INSERT INTO %s " +
                "(firstName, lastName, email, phoneNumber, active, " +
                "createdOn, idEnterprise, birthday)" +
                "VALUES (?, ?, ?, ?, ?, ?, ?, ?)",
                TABLE_NAME);

        MySqlConnectionService connectionService = new MySqlConnectionService(
                "localhost", "crm_db", "root", "123456");


        try (
            Connection connection = connectionService.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(sql)) {

            preparedStatement.setString(1, user.getFirstName());
            preparedStatement.setString(2, user.getLastName());
            preparedStatement.setString(3, user.getEmail());
            preparedStatement.setString(4, user.getPhoneNumber());
            preparedStatement.setInt(5, booleanToInt(user.isActive()));
            preparedStatement.setDate(6, Date.valueOf(user.getCreatedOn()));
            preparedStatement.setInt(7, user.getIdEnterprise());
            preparedStatement.setDate(8, Date.valueOf(user.getBirthday()));
            preparedStatement.executeUpdate();

        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    private void update(User user){
        String sql = String.format("UPDATE %s SET " +
                "firstName='%s', lastName='%s', email='%s'," +
                "phoneNumber='%s', active=%d, createdOn='%s', idEnterprise=%d," +
                "birthday='%s' WHERE id=%d",
                TABLE_NAME,
                user.getFirstName(),
                user.getLastName(),
                user.getEmail(),
                user.getPhoneNumber(),
                booleanToInt(user.isActive()),
                user.getCreatedOn(),
                user.getIdEnterprise(),
                user.getBirthday(),
                user.getId());
        MySqlConnectionService connectionService = new MySqlConnectionService(
                "localhost", "crm_db", "root", "123456");


        try ( Connection connection = connectionService.getConnection();
                Statement statement = connection.createStatement()) {

            statement.executeUpdate(sql);

        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    @Override
    public void save(User user) {
        if (findById(user.getId()) != null) {
            update(user);
        } else {
            insert(user);
        }
    }
}
