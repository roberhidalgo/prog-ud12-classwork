package es.progcipfpbatoi.modelo.dao;

import es.progcipfpbatoi.modelo.dto.User;

public interface UserDAOInterface {

    User findById(int id);
    void save(User user);
}
