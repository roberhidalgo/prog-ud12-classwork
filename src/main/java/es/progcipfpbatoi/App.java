package es.progcipfpbatoi;

import es.progcipfpbatoi.controlador.Actividad4Controller;
import es.progcipfpbatoi.controlador.ChangeScene;
import es.progcipfpbatoi.modelo.dao.DBEnterpriseDAO;
import es.progcipfpbatoi.modelo.dao.DBUserDAO;
import javafx.application.Application;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * Hello world!
 *
 */
public class App extends Application
{
    @Override
    public void start(Stage stage) throws IOException {
        DBEnterpriseDAO dbEnterpriseDAO = new DBEnterpriseDAO();
        DBUserDAO dbUserDAO = new DBUserDAO();
        ChangeScene.change(stage, new Actividad4Controller(
                dbEnterpriseDAO, dbUserDAO),
                "/vista/form_actividad4.fxml");
    }

    public static void main(String[] args) {
        launch();
    }
}
