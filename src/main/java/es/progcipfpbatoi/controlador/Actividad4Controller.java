package es.progcipfpbatoi.controlador;

import es.progcipfpbatoi.modelo.dao.EnterpriseDAOInterface;
import es.progcipfpbatoi.modelo.dao.UserDAOInterface;
import es.progcipfpbatoi.modelo.dto.Enterprise;
import es.progcipfpbatoi.modelo.dto.User;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import org.controlsfx.validation.ValidationSupport;
import org.controlsfx.validation.Validator;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Actividad4Controller implements GenericController{


    private static final String[] POSIBLES_NOMBRES = {"Manuel", "Antonio", "María", "Germán", "Julián"};
    private static final String[] POSIBLES_APELLIDOS = {"Martínez", "Pérez", "López", "Hernández", "Palomo"};


    @FXML
    private TextField textFieldNombre;

    @FXML
    private TextField textFieldApellidos;

    @FXML
    private TextField textFieldEmail;

    @FXML
    private TextField textFieldTelefono;

    @FXML
    private CheckBox checkBoxActivo;

    @FXML
    private DatePicker datePickerBirthday;

    @FXML
    private ComboBox<Enterprise> comboBoxEmpresa;

    @FXML
    private Button buttonAnyadir;

    private EnterpriseDAOInterface enterpriseDAO;
    private UserDAOInterface userDAO;


    public Actividad4Controller(
            EnterpriseDAOInterface enterpriseDAO,
            UserDAOInterface userDAO) {
        this.enterpriseDAO = enterpriseDAO;
        this.userDAO = userDAO;
    }

    @Override
    public void initialize() {

        List<Enterprise> empresas = this.enterpriseDAO.findAll();
        this.comboBoxEmpresa.getItems().addAll(empresas);
    }

    @FXML
    private void handleButtonAnyadir() {
        String nombre = textFieldNombre.getText();
        String apellidos = textFieldApellidos.getText();
        String email = textFieldEmail.getText();
        String telefono = textFieldTelefono.getText();
        boolean estaActivo = checkBoxActivo.isSelected();
        LocalDate birthday = datePickerBirthday.getValue();
        int idEmpresa = comboBoxEmpresa.getSelectionModel().getSelectedItem().getId();

        User user = new User(nombre, apellidos, email,
                telefono, estaActivo, idEmpresa, birthday);

        // filtrado previo, a partir del email --> findByEmail (DAO)
        this.userDAO.save(user);
    }

    @FXML
    private void handleButtonAleatorio() {
        User usuario = generarUserAleatorio();
        this.userDAO.save(usuario);
    }

    private User generarUserAleatorio() {

        return new User(
                generarNombreAleatorio(),
                generarApellidoAleatorio(),
                generarEmailAleatorio(),
                generarBooleanAleatorio(),
                generarIdEmpresaAleatoria(),
                generarLocalDateAleatorio());
    }

    private String generarNombreAleatorio() {
        return POSIBLES_NOMBRES[new Random().nextInt(POSIBLES_NOMBRES.length)];
    }

    private String generarApellidoAleatorio() {
        return POSIBLES_APELLIDOS[new Random().nextInt(POSIBLES_APELLIDOS.length)];
    }

    private String generarEmailAleatorio() {
        return new Random().nextInt() + "@testbatoi.com";
    }

    private boolean generarBooleanAleatorio() {
        int numero = new Random().nextInt(2);
        return numero == 1;
    }

    private LocalDate generarLocalDateAleatorio() {
        return LocalDate.of(1990, 1, new Random().nextInt(31) + 1);
    }

    private int generarIdEmpresaAleatoria() {
        return new Random().nextInt(5) + 1;
    }
}
